package com.quicknote.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    private static final String PREFERENCE_APP_LAUNCHES = "app_launches";
    private static final String PREFERENCE_PREMIUM = "premium";

    public static int getAppLaunches(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).
                getInt(PREFERENCE_APP_LAUNCHES, 0);
    }

    public static void setAppLaunches(Context context, int appLaunches) {
        SharedPreferences.Editor editor = PreferenceManager.
                getDefaultSharedPreferences(context).edit();
        editor.putInt(PREFERENCE_APP_LAUNCHES, appLaunches);
        editor.apply();
    }

    public static boolean isPremium(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).
                getBoolean(PREFERENCE_PREMIUM, false);
    }

    public static void setPremium(Context context, boolean premium) {
        SharedPreferences.Editor editor = PreferenceManager.
                getDefaultSharedPreferences(context).edit();
        editor.putBoolean(PREFERENCE_PREMIUM, premium);
        editor.apply();
    }
}
