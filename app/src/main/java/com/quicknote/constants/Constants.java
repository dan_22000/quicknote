package com.quicknote.constants;

public class Constants {

    public static final String INTENT_PREMIUM_STATUS_CHANGED = "INTENT_PREMIUM_STATUS_CHANGED";
    public static final String INTENT_EXTRA_ID = "id";
    public static final int INVALID_VALUE = -1;
    public static final int REQUEST_CODE_LOCATION = 501;
    public static final int REQUEST_CODE_INAPP = 502;
}
