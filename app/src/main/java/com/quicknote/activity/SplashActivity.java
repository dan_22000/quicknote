package com.quicknote.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.quicknote.R;
import com.quicknote.preferences.Preferences;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button;
    private ImageView cancel;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Find views by id
        ImageView icon = findViewById(R.id.icon);
        cancel = findViewById(R.id.cancel);
        button = findViewById(R.id.button);

        // Set listener
        cancel.setOnClickListener(this);
        button.setOnClickListener(this);

        // Animate icon
        Animation rotation = AnimationUtils.
                loadAnimation(this, R.anim.rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        icon.startAnimation(rotation);

        // AdMob Interstitial Ad
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial_test_id));
        interstitialAd.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onClick(View view) {
        if (view.equals(button)) {
            Toast.makeText(this,
                    getString(R.string.message_logged_in),
                    Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

            if (!Preferences.isPremium(this)) {
                interstitialAd.show();
            }
            finish();
        } else if (view.equals(cancel)) {
            finish();
        }
    }
}
