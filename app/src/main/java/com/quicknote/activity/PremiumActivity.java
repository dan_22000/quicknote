package com.quicknote.activity;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.android.vending.billing.IInAppBillingService;
import com.quicknote.R;
import com.quicknote.constants.Constants;
import com.quicknote.preferences.Preferences;

import org.json.JSONObject;

public class PremiumActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PREMIUM_SERVICE_INTENT_ACTION = "com.android.vending.billing.InAppBillingService.BIND";
    public static final String PREMIUM_SERVICE_INTENT_PACKAGE = "com.android.vending";
    public static final String PREMIUM_SKU = "premium";
    public static final String PREMIUM_BUY_INTENT_TYPE = "inapp";
    public static final String PREMIUM_DEVELOPER_PAYLOAD = "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ";
    public static final String PREMIUM_BUY_INTENT_BUNDLE_ID = "BUY_INTENT";
    public static final String PREMIUM_PURCHASE_DATA_EXTRA = "INAPP_PURCHASE_DATA";
    public static final String PREMIUM_PRODUCT_ID = "productId";
    public static final int PREMIUM_API_VERSION = 3;

    private Button button;
    private View cancel;
    private IInAppBillingService billingService;
    private ServiceConnection serviceConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);

        // Find views by id
        button = findViewById(R.id.button);
        cancel = findViewById(R.id.cancel);

        // Set listener
        button.setOnClickListener(this);
        cancel.setOnClickListener(this);

        initInappBilling();
    }

    private void initInappBilling() {
        serviceConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                billingService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                billingService = IInAppBillingService.Stub.asInterface(service);
            }
        };

        Intent serviceIntent = new Intent(PREMIUM_SERVICE_INTENT_ACTION);
        serviceIntent.setPackage(PREMIUM_SERVICE_INTENT_PACKAGE);
        bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
    }

    private void handlePurchase() {
        try {
            Bundle buyIntentBundle = billingService.getBuyIntent(PREMIUM_API_VERSION, getPackageName(),
                    PREMIUM_SKU, PREMIUM_BUY_INTENT_TYPE, PREMIUM_DEVELOPER_PAYLOAD);

            PendingIntent pendingIntent = buyIntentBundle.getParcelable(PREMIUM_BUY_INTENT_BUNDLE_ID);

            startIntentSenderForResult(pendingIntent.getIntentSender(),
                    Constants.REQUEST_CODE_INAPP, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                    Integer.valueOf(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE_INAPP) {
            String purchaseData = data.getStringExtra(PREMIUM_PURCHASE_DATA_EXTRA);

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString(PREMIUM_PRODUCT_ID);

                    if (sku.equals(PREMIUM_SKU)) {
                        unlockPremium();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void unlockPremium() {
        Preferences.setPremium(this, true);
        sendBroadcast(new Intent(Constants.INTENT_PREMIUM_STATUS_CHANGED));
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (billingService != null) {
            unbindService(serviceConn);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.equals(button)) {
            handlePurchase();

            // Unlock Premium every time we click on the button for testing
            unlockPremium();
        } else if (view.equals(cancel)) {
            finish();
        }
    }
}
