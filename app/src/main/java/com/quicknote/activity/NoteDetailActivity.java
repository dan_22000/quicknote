package com.quicknote.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.quicknote.R;
import com.quicknote.constants.Constants;
import com.quicknote.database.DatabaseHelper;
import com.quicknote.entity.Note;

public class NoteDetailActivity extends AppCompatActivity implements DialogInterface.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private TextView noteTitle;
    private TextView noteMessage;
    private Note note;
    private CheckBox status;
    private long noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        // Find views by id
        Toolbar toolbar = findViewById(R.id.toolbar);
        noteTitle = findViewById(R.id.note_title);
        noteMessage = findViewById(R.id.note_message);
        status = findViewById(R.id.status);
        status.setOnCheckedChangeListener(this);

        // Init toolbar
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get noteId from Intent
        noteId = getIntent().getLongExtra(Constants.INTENT_EXTRA_ID, 0);
    }

    private void updateView() {
        note = DatabaseHelper.getInstance(this).getNote(noteId);
        if (note != null) {
            noteTitle.setText(note.getTitle());
            noteMessage.setText(note.getMessage());
            status.setChecked(note.getStatus() == Note.STATUS_DONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();

            return true;
        } else if (itemId == R.id.edit) {
            Intent intent = new Intent(this, NoteEditActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_ID, note.getId());
            startActivity(intent);

            return true;
        } else if (itemId == R.id.delete) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.dialog_delete_note_message)
                    .setNegativeButton(R.string.button_no_text, null)
                    .setPositiveButton(R.string.button_yes_text, NoteDetailActivity.this)
                    .show();
        }

        return false;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Toast.makeText(this, getString(R.string.message_note_deleted), Toast.LENGTH_SHORT).show();
        DatabaseHelper.getInstance(this).deleteNote(note.getId());
        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        note.setStatus(checked ? Note.STATUS_DONE : Note.STATUS_TODO);
        DatabaseHelper.getInstance(this).updateNote(note);
    }
}
