package com.quicknote.activity;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.quicknote.R;
import com.quicknote.adapter.NoteAdapter;
import com.quicknote.constants.Constants;
import com.quicknote.database.DatabaseHelper;
import com.quicknote.entity.Note;
import com.quicknote.preferences.Preferences;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, DialogInterface.OnClickListener {

    private NoteAdapter adapter;
    private AdView adView;
    private Receiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find views by id
        adView = findViewById(R.id.ad_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ListView listView = findViewById(R.id.list);
        FloatingActionButton floatingActionButton = findViewById(R.id.floating_button);

        // Set listener
        floatingActionButton.setOnClickListener(this);

        // Init toolbar
        setSupportActionBar(toolbar);

        // Init objects
        adapter = new NoteAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        // Handle app launches
        int appLaunches = Preferences.getAppLaunches(this);
        appLaunches = appLaunches + 1;
        Preferences.setAppLaunches(this, appLaunches);
        if (appLaunches >= 3) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.dialog_rate_message)
                    .setNegativeButton(R.string.button_no_text, null)
                    .setPositiveButton(R.string.button_yes_text, MainActivity.this)
                    .show();
        }

        // Show AdMob Banner
        AdRequest adRequest = new AdRequest.Builder().build();
        if (!Preferences.isPremium(this)) {
            adView.loadAd(adRequest);
        }

        receiver = new Receiver();
        IntentFilter filter = new IntentFilter(Constants.INTENT_PREMIUM_STATUS_CHANGED);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();

        if (Preferences.isPremium(this)) {
            adView.setVisibility(View.INVISIBLE);
        }
    }

    private void updateView() {
        List<Note> notes = DatabaseHelper.getInstance(this).getAllNotes();
        adapter.setNotes(notes);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,
                NoteEditActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.premium) {
            Intent intent = new Intent(this,
                    PremiumActivity.class);
            startActivity(intent);

            return true;
        }

        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(this,
                NoteDetailActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_ID, id);
        startActivity(intent);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        String packageName = getPackageName();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            intent.setData(Uri.parse("https://market.android.com/details?id=" + packageName));

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e2) {
                e2.printStackTrace();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem premium = menu.findItem(R.id.premium);
        if (premium != null) {
            premium.setVisible(!Preferences.isPremium(this));
        }

        return super.onPrepareOptionsMenu(menu);
    }

    private class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            supportInvalidateOptionsMenu();
        }
    }
}
