package com.quicknote.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.quicknote.R;
import com.quicknote.constants.Constants;
import com.quicknote.database.DatabaseHelper;
import com.quicknote.entity.Note;

public class NoteEditActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    private EditText title;
    private EditText message;
    private Note note;
    private LocationManager locationManager;
    private Location noteLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_edit);

        // Find views by id
        Toolbar toolbar = findViewById(R.id.toolbar);
        Button button = findViewById(R.id.button);
        title = findViewById(R.id.title);
        message = findViewById(R.id.message);

        // Init toolbar
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Set listener
        button.setOnClickListener(this);

        // Init objects
        long id = getIntent().getLongExtra(Constants.INTENT_EXTRA_ID, Constants.INVALID_VALUE);
        if (id != Constants.INVALID_VALUE) {
            note = DatabaseHelper.getInstance(this).getNote(id);
        }
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Handle user location
        checkLocationPermission();
        getLastKnownLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }

    private void getLastKnownLocation() {
        String locationProvider = LocationManager.GPS_PROVIDER;
        // Or use LocationManager.GPS_PROVIDER

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        noteLocation = locationManager.getLastKnownLocation(locationProvider);
    }

    private void checkLocationPermission() {
        // Android Marshmallow Location Permission Check
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            try {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constants.REQUEST_CODE_LOCATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            requestLocationUpdates();
        }
    }

    private void requestLocationUpdates() {
        // Register the listener with the Location Manager to receive location updates
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    private void updateView() {
        if (note != null) {
            title.setText(note.getTitle());
            message.setText(note.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE_LOCATION) {
            requestLocationUpdates();
            getLastKnownLocation();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();

            return true;
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        String titleString = title.getText().toString();
        String messageString = message.getText().toString();

        if (note == null) {
            Note newNote = new Note();
            newNote.setTitle(titleString);
            newNote.setMessage(messageString);
            newNote.setTimestamp(System.currentTimeMillis());
            newNote.setStatus(Note.STATUS_TODO);

            if (noteLocation != null) {
                newNote.setLatitude(noteLocation.getLatitude());
                newNote.setLongitude(noteLocation.getLongitude());
            }
            DatabaseHelper.getInstance(this).insertNote(newNote);
        } else {
            note.setTitle(titleString);
            note.setMessage(messageString);
            DatabaseHelper.getInstance(this).updateNote(note);
        }

        Toast.makeText(this, getString(R.string.message_note_saved), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onLocationChanged(Location location) {
        noteLocation = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
